package com.abnamro.minicouch.utils

import java.math.BigDecimal

/**
 * https://en.wikipedia.org/wiki/International_Bank_Account_Number#Validating_the_IBAN
 */
object IBAN {
    private val COUNTRY_CODE = "NL"
    private val BIC = "MICO"
    private val IBAN_LENGTH = 18
    private val leftLimit = 0L
    private val rightLimit = 10000000000L //under 100.000.000.000, so less then 11 numbers

    fun validateIBAN(iban: String): Boolean {
        val strippedFromSpacesIban = iban.replace(" ", "")
        if (strippedFromSpacesIban.length == IBAN_LENGTH) {
            if (calculateRemainderOfIBan(strippedFromSpacesIban) == 1) {
                return true
            } else {
                println("Remainder != 1")
            }
        }
        return false
    }

    /**
     *
     * Calculates the remainder
     *
     * 1. Check that the total IBAN length is correct as per the country. If not, the IBAN is invalid
     * 2. Move the four initial characters to the end of the string
     * 3. Replace each letter in the string with two digits, thereby expanding the string, where A = 10, B = 11, ..., Z = 35
     * 4. Interpret the string as a decimal integer and compute the remainder of that number on division by 97
     * @return if the return equals 1, it is a valid IBAN.
     *
     */
    private fun calculateRemainderOfIBan(iban: String): Int {
        if (iban.length == IBAN_LENGTH) {
            val front = iban.substring(0, 4)
            val back = iban.substring(4)
            val backFront = back + front
            var ibanToDec = ""
            backFront.forEach { c -> ibanToDec += if (c.isLetter()) c.toInt() else c }
            val remainder = BigDecimal(ibanToDec).rem(BigDecimal("97"))
            return remainder.toInt()
        } else {
            throw Exception("IBan != $IBAN_LENGTH")
        }
    }


    /**
     * Generates an IBAN Between NL ** {MICO 0000 0000 00} and NL ** {MICO 9999 9999 99}
     *
     * 1. Generate a number under 10.000.000.000
     * 2. Sets the control number to 00
     * 3. Moves the four initial characters to the end of the string.
     * 4. Replaces the letters in the string with digits, expanding the string as necessary, such that A or a = 10, B or b = 11, and Z or z = 35. Each alphabetic character is therefore replaced by 2 digits
     * 5. Converts the string to an long (i.e. ignores leading zeroes).
     * 6. Calculates mod-97 of the new number, which results in the remainder. @see IBAN.calculateRemainderOfIBan()
     * 7. Subtract the remainder from 98, and use the result for the two check digits. If the result is a single digit number, pad it with a leading 0 to make a two-digit number.
     *
     */
    fun generateRandomIBAN(): String {
        val formattedNumber = String.format("%010d", leftLimit + (Math.random() * (rightLimit - leftLimit)).toLong())
        val newControlNumber = 98 - calculateRemainderOfIBan("$COUNTRY_CODE${"00"}$BIC$formattedNumber")
        return "$COUNTRY_CODE${String.format("%02d", newControlNumber)}$BIC$formattedNumber"
    }
}