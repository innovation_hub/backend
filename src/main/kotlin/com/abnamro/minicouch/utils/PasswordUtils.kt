package com.abnamro.minicouch.utils

import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.security.spec.InvalidKeySpecException
import java.util.*
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec
import javax.xml.bind.DatatypeConverter

/**
 * A utility class to hash passwords and check passwords vs hashed values. It uses a combination of hashing and unique
 * salt. The algorithm used is PBKDF2WithHmacSHA1 which, although not the best for hashing passwordHash (vs. bcrypt) is
 * still considered robust and [ recommended by NIST ](https://security.stackexchange.com/a/6415/12614).
 * The hashed event has 256 bits.
 */
object PasswordUtils {

    private val RANDOM = SecureRandom()
    private val ITERATIONS = 10000
    private val KEY_LENGTH = 256

    /**
     * Returns a random salt to be used to hash a passwordHash.
     *
     * @return a 16 bytes random salt
     */
    val nextSalt: String
        get() {
            val salt = ByteArray(16)
            RANDOM.nextBytes(salt)
            return DatatypeConverter.printBase64Binary(salt)
        }

    /**
     * Returns a salted and hashed pw using the provided hash.<br></br>
     * Note - side effect: the passwordHash is destroyed (the char[] is filled with zeros)
     *
     * @param password the passwordHash to be hashed
     * @param salt     a 16 bytes salt, ideally obtained with the getNextSalt method
     *
     * @return the passwordHash pw with a pinch of salt
     */
    fun hash(password: String, salt: String): String {
        val pw = password.toCharArray()
        val s = DatatypeConverter.parseBase64Binary(salt)

        val spec = PBEKeySpec(pw, s, ITERATIONS, KEY_LENGTH)
        Arrays.fill(pw, Character.MIN_VALUE)
        try {
            val skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1")
            return DatatypeConverter.printBase64Binary(skf.generateSecret(spec).encoded)
        } catch (e: NoSuchAlgorithmException) {
            throw AssertionError("Error while hashing a pw: " + e.message, e)
        } catch (e: InvalidKeySpecException) {
            throw AssertionError("Error while hashing a pw: " + e.message, e)
        } finally {
            spec.clearPassword()
        }
    }

    /**
     * Returns true if the given passwordHash and userSalt match the hashed event, false otherwise.<br></br>
     * Note - side effect: the passwordHash is destroyed (the char[] is filled with zeros)
     *
     * @param password     the passwordHash to check
     * @param userSalt         the userSalt used to hash the passwordHash
     * @param expectedHash the expected hashed event of the passwordHash
     *
     * @return true if the given passwordHash and userSalt match the hashed event, false otherwise
     */
    fun isExpectedPassword(password: String, userSalt: String, expectedHash: String): Boolean {
        val pwHash = DatatypeConverter.parseBase64Binary(expectedHash)
        val pw = password.toCharArray()
        val pwdHash = DatatypeConverter.parseBase64Binary(hash(password, userSalt))
        Arrays.fill(pw, Character.MIN_VALUE)
        if (pwdHash.size != pwHash.size) return false
        for (i in pwdHash.indices) {
            if (pwdHash[i] != pwHash[i]) return false
        }
        return true
    }

    /**
     * Generates a random passwordHash of a given length, using letters and digits.
     *
     * @param length the length of the passwordHash
     *
     * @return a random passwordHash
     */
    fun generateRandomPassword(length: Int): String {
        val sb = StringBuilder(length)
        for (i in 0..length - 1) {
            val c = RANDOM.nextInt(62)
            if (c <= 9) {
                sb.append(c.toString())
            } else if (c < 36) {
                sb.append(('a' + c - 10).toChar())
            } else {
                sb.append(('A' + c - 36).toChar())
            }
        }
        return sb.toString()
    }
}