package com.abnamro.minicouch.database.tables

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

/**
 * Not used YET!
 */

class Company(id: EntityID<Int>) : IntEntity(id) {
    object Table : IntIdTable() {
        override val tableName: String get() = "company"
        val name = varchar("name", 150).index(isUnique = true)
    }

    companion object : IntEntityClass<Company>(Table)

    var name by Table.name
}