package com.abnamro.minicouch.database.tables

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable


class PersonCompanyEmployment(id: EntityID<Int>) : IntEntity(id) {
    object Table : IntIdTable() {
        override val tableName: String get() = "person_company_employment"
        val person = reference("person_id", Person.table)
        val company = reference("company_id", Company.table)
        val employeeEmail = varchar("employee_email", 150)
    }

    companion object : IntEntityClass<PersonCompanyEmployment>(Table)

    var person by Person referencedOn Table.person
    var company by Company referencedOn Table.company
    var employeeEmail by Table.employeeEmail
}