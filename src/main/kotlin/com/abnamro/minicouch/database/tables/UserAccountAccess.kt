package com.abnamro.minicouch.database.tables

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable


class UserAccountAccess(id: EntityID<Int>) : IntEntity(id) {
    object Table : IntIdTable() {
        override val tableName: String get() = "user_account_access"
        val account = reference("account_id", Account.Table)
        val user = reference("user_id", User.Table)
    }

    companion object : IntEntityClass<UserAccountAccess>(Table)

    var account by Account referencedOn Table.account
    var user by User referencedOn Table.user
}