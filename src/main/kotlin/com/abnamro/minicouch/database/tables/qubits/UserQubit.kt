package com.abnamro.minicouch.database.tables.qubits

import com.abnamro.minicouch.database.caretakers.QubitCaretaker
import com.abnamro.minicouch.database.caretakers.waitForNotNull
import com.abnamro.minicouch.database.tables.ActionType
import com.abnamro.minicouch.database.tables.User
import com.abnamro.minicouch.services.ExceptionEnum
import com.abnamro.minicouch.services.MiniCouchException
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction


class UserQubit(id: EntityID<Int>) : IntEntity(id), IQubit {
    var isTrue by Table.isTrue
    var note by Table.note

    var actionType by ActionType referencedOn Table.actionType
    var user by User referencedOn Table.user

    override fun state(): Boolean {
        return isTrue
    }

    object Table : IntIdTable() {
        override val tableName: String get() = "qubit_user"
        val isTrue = bool("is_true")
        val note = varchar("note", 255)

        val actionType = reference("action_type_id", ActionType.table)
        val user = reference("user_id", User.table)
    }

    companion object : IntEntityClass<UserQubit>(Table) {
        /**
         *
         * Every Qubit should have a similar function in their companion object for easy access.
         *
         * @see User
         * @see ActionType
         * @return the Qubit for a user on a certain ActionType
         */
        fun getQubit(actionType: ActionType, user: User?): UserQubit? {
            if (user == null) {
                return null
            }
            var success: Boolean? = null
            var qubit: UserQubit? = null
            var exception: Exception? = null
            transaction {
                try {
                    qubit = UserQubit.find { UserQubit.Table.actionType eq actionType.id.value and (UserQubit.Table.user eq user.id.value) }.firstOrNull()
                    success = true
                } catch (e: Exception) {
                    exception = e
                }
            }
            waitForNotNull({ success }, { exception }, maxWaitTime = 5000)
            val e = exception
            if (e != null) {
                throw MiniCouchException("errss :", ExceptionEnum.GENERIC)
            }
            return qubit
        }

        /**
         * Checks if all the Qubits are still necessary for this user. Add / edit / remove where required.
         */
        fun resetQubits(u: User) {
            transaction {
                val person = u.ownerPerson
                if (person != null) {
                    val verified = person.verifyMandatoryLegalFilled()
                    val qUser = UserQubit.getQubit(QubitCaretaker.ACTION_TYPE_OPEN_ACCOUNT, u)
                    if (qUser != null) {
                        if (verified) {
                            qUser.isTrue = true
                            qUser.note = "We know you"
                        } else {
                            qUser.isTrue = false
                            qUser.note = "not enough personal information after edit"
                        }
                    } else {
                        //could not find record
                        UserQubit.new {
                            this.actionType = QubitCaretaker.ACTION_TYPE_OPEN_ACCOUNT
                            this.user = u
                            this.isTrue = false
                            if (verified) {
                                this.isTrue = true
                                this.note = "We know you"
                            } else {
                                this.isTrue = false
                                this.note = "not enough personal information after edit"
                            }
                        }
                    }
                } else {
                    //company personal should not have access at all.
                    //TODO: Some company personal DO need access, how will we give them access?
                    //val companyPerson = u.ownerPersonCompany?.person
                    //person from company has NO access in creating account!
                    //create / update to FALSE!
                    val reason = "You are only working here, don't get cocky!"
                    val qUser = UserQubit.getQubit(QubitCaretaker.ACTION_TYPE_OPEN_ACCOUNT, u)
                    if (qUser != null) {
                        qUser.isTrue = false
                        qUser.note = reason
                    } else {
                        //could not find record
                        UserQubit.new {
                            this.actionType = QubitCaretaker.ACTION_TYPE_OPEN_ACCOUNT
                            this.user = u
                            this.isTrue = false
                            this.note = reason
                        }
                    }
                }
            }
        }
    }
}