package com.abnamro.minicouch.database.tables.qubits

class InputList(private val inputModels: ArrayList<Any>) {

    fun addInput(vararg input: Any) {
        inputModels.addAll(input)
    }

    fun <CLASS> findInput(t: Class<CLASS>): CLASS? = inputModels.firstOrNull { it.javaClass == t }?.let { it as CLASS? }
}
