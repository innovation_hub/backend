package com.abnamro.minicouch.database.tables

import com.abnamro.minicouch.database.DBUtils
import com.abnamro.minicouch.services.ExceptionEnum
import com.abnamro.minicouch.services.MiniCouchException
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable


class User(id: EntityID<Int>) : IntEntity(id) {

    object Table : IntIdTable() {
        override val tableName: String get() = "user"

        val ownerPerson = optReference("person_id", Person.Table)
        val ownerPersonCompanyEmployment = optReference("person_company_employment_id", PersonCompanyEmployment.Table)

        /**
         * Add mutual exclusive disjunction. One Must be null while the other one mustn't
         */
        override fun createStatement(): List<String> {
            val list = ArrayList(super.createStatement())
            list.add(DBUtils.mutualExclusiveTrigger(ownerPerson, ownerPersonCompanyEmployment, DBUtils.TriggerOn.INSERT))
            list.add(DBUtils.mutualExclusiveTrigger(ownerPerson, ownerPersonCompanyEmployment, DBUtils.TriggerOn.UPDATE))
            return list
        }
    }

    companion object : IntEntityClass<User>(Table)

    //owner
    var ownerPerson by Person optionalReferencedOn Table.ownerPerson
    var ownerPersonCompany by PersonCompanyEmployment optionalReferencedOn Table.ownerPersonCompanyEmployment

    fun getPerson(): Person {
        val p = ownerPerson

        return if (p != null) {
            p
        } else {
            val pc = ownerPersonCompany
            return pc?.person ?: throw MiniCouchException(ExceptionEnum.ACCOUNT_CONFLICT)
        }
    }

    fun isPersonOwned(): Boolean {
        return if (ownerPerson != null) {
            true
        } else {
            if (ownerPersonCompany != null) {
                false
            } else {
                throw MiniCouchException(ExceptionEnum.ACCOUNT_CONFLICT)
            }
        }
    }
}