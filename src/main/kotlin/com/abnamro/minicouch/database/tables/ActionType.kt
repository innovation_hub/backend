package com.abnamro.minicouch.database.tables

import com.abnamro.minicouch.database.caretakers.handleReturnOrException
import com.abnamro.minicouch.database.caretakers.waitForNotNull
import com.abnamro.minicouch.database.tables.qubits.*
import com.abnamro.minicouch.services.ExceptionEnum
import com.abnamro.minicouch.services.MiniCouchException
import com.abnamro.minicouch.services.authorisation.SimpleCredential
import com.abnamro.minicouch.services.logger
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.transactions.transaction

class ActionType(id: EntityID<Int>) : IntEntity(id) {
    object Table : IntIdTable() {
        override val tableName: String get() = "action_type"
        val actionCode = varchar("action_code", 10).index(isUnique = true)
        val actionName = varchar("action_name", 255)
        val minimumCredentialRating = integer("minimum_credential_rating")
        val actionDescription = varchar("description", 512).nullable()

        val qubitAccountType = bool("qubit_account_type").nullable()
        val qubitAccount = bool("qubit_account").nullable()
        val qubitUserAccount = bool("qubit_user_account").nullable()
        val qubitUser = bool("qubit_user").nullable()

        override fun createStatement(): List<String> {
            ActionType.new {
                this.code = "OA"
                this.name = "Open Account"
                this.qubitAccountType = null
                this.qubitAccount = null
                this.qubitUserAccount = null

                this.qubitUser = false

                this.minCredRating = 1
                this.description = "Creates an Account for a User, requires a rating of minimal 2. Does not require quantum bit"
            }

            //PERSON
            ActionType.new {
                this.code = "CP"
                this.name = "Change Person"

                this.qubitAccountType = null
                this.qubitAccount = null
                this.qubitUserAccount = null

                this.qubitUser = false


                this.minCredRating = 1
                this.description = "Changes person information, requires a rating of minimal 1. Does not require quantum bit"
            }
            ActionType.new {
                this.code = "VP"
                this.name = "View Person"

                this.qubitAccountType = null
                this.qubitAccount = null
                this.qubitUserAccount = null
                this.qubitUser = true

                this.minCredRating = 1
                this.description = "View your person information, requires a rating of minimal 1. Does not require quantum bit"
            }


            return super.createStatement()
        }
    }

    companion object : IntEntityClass<ActionType>(Table)

    var code by Table.actionCode
    var name by Table.actionName
    var minCredRating by Table.minimumCredentialRating
    var description by Table.actionDescription

    var qubitAccountType by Table.qubitAccountType
    var qubitAccount by Table.qubitAccount
    var qubitUserAccount by Table.qubitUserAccount
    var qubitUser by Table.qubitUser


    /**
     * @return error message, if this field is null, the check is successful.
     */
    fun checkQubits(vararg inputs: Any): String? {

        val input = InputList(ArrayList(inputs.toList()))
        //val account = input.findInput(Account::class.java)
        //val userAccount = input.findInput(UserAccountAccess::class.java)
        val user = input.findInput(User::class.java)


        val qu1 = true //qubitAccountType == null || UserQubit.Table.getQubit(actionType!!, user!!)?.state ?: true
        val qu2 = true //qubitAccount == null || UserQubit.Table.getQubit(actionType!!, user!!)?.state ?: true
        val qu3 = true //qubitUserAccount == null || UserQubit.Table.getQubit(actionType!!, user!!)?.state ?: true

        try {
            val qb4 = UserQubit.getQubit(this, user!!)

            val b4 = qubitUser == null || qb4?.isTrue ?: !qubitUser!!

            return if (qu1 && qu2 && qu3 && b4) {
                null
            } else {
                "Errors: " + collectNotes(qb4 to "User")
            }

        } catch (npe: NullPointerException) {
            throw MiniCouchException("Null pointer on Input", ExceptionEnum.GENERIC)
        } catch (e: Exception) {
            logger().warning("errorrr: " + e)
            throw  e
        }
    }

    /**
     * Retrieves the notes on why a Quantum bit blocks
     */
    private fun collectNotes(vararg qubits: Pair<UserQubit?, String>): String {
        var notes: String? = null
        var exception: Exception? = null

        transaction {
            var n = ""
            try {
                qubits.forEach {
                    val q = it.first
                    if (n.isNotEmpty()) n += ", "
                    n += if (q == null) {
                        "${it.second} no access"
                    } else {
                        "{${q.actionType.name}}:${q.note}"
                    }
                }
                notes = n
            } catch (e: Exception) {
                exception = e
            }
        }
        waitForNotNull({ notes }, { exception })
        return handleReturnOrException(notes, MiniCouchException(ExceptionEnum.QUANTUM_BOOLEAN_COLLECT_NOTES), exception)
    }


    /**
     * Adds all your credentials to create a total rating
     * @return true If your rating is enough
     */
    fun checkMinimumCredentials(credentials: List<SimpleCredential>) = credentials.sumBy { it.level } >= minCredRating


    /**
     * Still testing
     */
    fun validateAction(vararg input: Any): Boolean {

        //inputs
        val user = findInput(User::class.java, input)
        val account = findInput(Account::class.java, input)


        val qubitChecks = ArrayList<Pair<Boolean?, () -> IQubit?>>()
        qubitChecks.add(this.qubitUser to { UserQubit.getQubit(this, user) })
        qubitChecks.add(this.qubitAccount to { AccountQubit.getQubit(this, account) })

        var valid = true
        qubitChecks.forEach { valid = valid && check(it.first, it.second) }

        return valid

    }

    private fun check(actionRequiresQubit: Boolean?, qubitF: () -> IQubit?): Boolean {
        return if (actionRequiresQubit == null) {
            true
        } else {
            val qubit = qubitF.invoke()
            if (actionRequiresQubit) {
                qubit?.state() ?: false
            } else {
                qubit?.state() ?: true
            }
        }
    }

    private fun <CLASS> findInput(t: Class<CLASS>, input: Array<out Any>): CLASS? = input.firstOrNull { it.javaClass == t }?.let { it as CLASS? }
}

