package com.abnamro.minicouch.database.tables

import com.abnamro.minicouch.database.DBUtils
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

class AccountHolder(id: EntityID<Int>) : IntEntity(id) {
    object Table : IntIdTable() {
        override val tableName: String get() = "account_holder"

        val accountID = reference("account_id", Account.table)
        val personID = optReference("person_id", Person.table)
        val companyID = optReference("company_id", Company.table)

        /**
         * Add mutual exclusive disjunction. One Must be null while the other one mustn't
         */
        override fun createStatement(): List<String> {
            val list = ArrayList(super.createStatement())
            list.add(DBUtils.mutualExclusiveTrigger(Table.personID, Table.companyID, DBUtils.TriggerOn.INSERT))
            list.add(DBUtils.mutualExclusiveTrigger(Table.personID, Table.companyID, DBUtils.TriggerOn.UPDATE))
            return list
        }
    }

    companion object : IntEntityClass<AccountHolder>(Table)

    var account by Account referencedOn Table.accountID

    var person by Person optionalReferencedOn Table.personID
    var company by Company optionalReferencedOn Table.companyID
}