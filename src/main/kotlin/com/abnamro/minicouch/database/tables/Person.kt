package com.abnamro.minicouch.database.tables

import com.abnamro.minicouch.database.caretakers.handleReturnOrException
import com.abnamro.minicouch.services.ExceptionEnum
import com.abnamro.minicouch.services.MiniCouchException
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.transactions.transaction
import java.text.DateFormat


/**
 * @see SimplePerson for REST services
 */
class Person(id: EntityID<Int>) : IntEntity(id) {
    object Table : IntIdTable() {
        override val tableName: String get() = "person"

        val email = varchar("email", 150).index(isUnique = true)
        val firstName = varchar("first_name", length = 150)
        val surname = varchar("surname", length = 150)
        val initials = varchar("initials", length = 20).default("")

        val birthDate = date("birth_date").nullable()
        val birthPlace = varchar("birth_place", 150).nullable()
    }

    companion object : IntEntityClass<Person>(Table)


    var email by Table.email
    var firstName by Table.firstName
    var initials by Table.initials
    var surname by Table.surname
    var birthDate by Table.birthDate
    var birthPlace by Table.birthPlace

    fun fullName(): String {
        //removes double spaces
        return "$firstName $initials $surname".replace(" +", " ")
    }

    fun verifyMandatoryLegalFilled(): Boolean {
        val bp = birthPlace
        return if (bp != null) {
            birthDate != null && birthPlace != null && bp.isNotEmpty()
        } else {
            false
        }
    }

    /**
     * @see User
     * @see PersonCompanyEmployment excluding this one for now!
     * @return users listed as owner, excluding the person company users
     */
    fun getAllUsers(): List<User> {
        var users: List<User>? = null
        var exception: Exception? = null
        transaction {
            try {
                users = User.find { User.Table.ownerPerson eq id }.toList()
            } catch (e: Exception) {
                exception = e
            }
        }
        return handleReturnOrException(users, MiniCouchException("test", ExceptionEnum.UNKNOWN), exception)
    }
}

/**
 * @see Person
 * This data class can be easily returned in REST services instead of EntityID classes
 */
data class SimplePerson(val name: String, val initials: String, val surname: String, val email: String, val birthDate: String?, val birthPlace: String?) {
    companion object {
        val sp = DateFormat.FULL
        fun init(person: Person) = SimplePerson(person.firstName, person.initials, person.surname, person.email, person.birthDate?.toString("yyyy-MM-dd"), person.birthPlace)
    }
}