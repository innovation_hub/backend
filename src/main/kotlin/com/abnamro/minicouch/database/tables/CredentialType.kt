package com.abnamro.minicouch.database.tables

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

class CredentialType(id: EntityID<Int>) : IntEntity(id) {
    object Table : IntIdTable() {
        val CREDENTIAL_EMAIL_PASSWORD = "EmailPassword"

        override val tableName: String get() = "credential_type"
        val type = varchar("type", 30).index(isUnique = true)
        val rating = integer("rating")

        override fun createStatement(): List<String> {
            CredentialType.new {
                this.rating = 1
                this.type = CREDENTIAL_EMAIL_PASSWORD
            }
            return super.createStatement()
        }
    }

    companion object : IntEntityClass<CredentialType>(Table)

    var type by Table.type
    var rating by Table.rating
}