package com.abnamro.minicouch.database.tables

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.joda.time.DateTime

class CredentialEmailPassword(id: EntityID<Int>) : IntEntity(id) {
    object Table : IntIdTable() {
        override val tableName: String get() = "credential_email_password"

        val email = varchar("email", 150).uniqueIndex()
        val salt = varchar("salt", 150)
        val passwordHash = varchar("password_hash", 150)
        val passwordUpdated = datetime("password_updated").default(DateTime.now())
    }

    companion object : IntEntityClass<CredentialEmailPassword>(Table)

    var email by CredentialEmailPassword.Table.email
    var salt by CredentialEmailPassword.Table.salt
    var passwordHash by CredentialEmailPassword.Table.passwordHash

    //TODO This should be checked when after checking token
    var passwordUpdated by CredentialEmailPassword.Table.passwordUpdated
}