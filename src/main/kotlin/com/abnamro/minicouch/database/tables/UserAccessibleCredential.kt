package com.abnamro.minicouch.database.tables

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

class UserAccessibleCredential(id: EntityID<Int>) : IntEntity(id) {
    object Table : IntIdTable() {
        override val tableName: String get() = "user_accessible_credential"
        val isOpen = bool("is_open").default(false)
        val credentialTableID = integer("credential_table_id")

        val userID = reference("user_id", User.Table)
        val credentialType = reference("credential_type_id", CredentialType.Table)
    }

    companion object : IntEntityClass<UserAccessibleCredential>(Table)

    //TODO: Check if the credential is not blocked!
    var isOpen by Table.isOpen
    var credentialTableID by Table.credentialTableID

    var user by User referencedOn Table.userID
    var credentialType by CredentialType referencedOn Table.credentialType
}