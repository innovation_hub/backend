package com.abnamro.minicouch.database.tables.qubits

import com.abnamro.minicouch.database.caretakers.waitForNotNull
import com.abnamro.minicouch.database.tables.Account
import com.abnamro.minicouch.database.tables.ActionType
import com.abnamro.minicouch.database.tables.User
import com.abnamro.minicouch.services.ExceptionEnum
import com.abnamro.minicouch.services.MiniCouchException
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction

class AccountQubit(id: EntityID<Int>) : IntEntity(id), IQubit {
    override fun state(): Boolean {
        return isTrue
    }

    object Table : IntIdTable() {
        override val tableName: String get() = "qubit_account"
        val isTrue = bool("is_true")
        val note = varchar("note", 255)

        val actionType = reference("action_type_id", ActionType.table)
        val account = reference("account_id", Account.table)
    }

    companion object : IntEntityClass<AccountQubit>(Table) {
        fun getQubit(actionType: ActionType, account: Account?): AccountQubit? {
            if (account == null) {
                return null
            }
            var success: Boolean? = null
            var qubit: AccountQubit? = null
            var exception: Exception? = null
            transaction {
                try {
                    qubit = find { Table.actionType eq actionType.id.value and (Table.account eq account.id.value) }.firstOrNull()
                    success = true
                } catch (e: Exception) {
                    exception = e
                }
            }

            waitForNotNull({ success }, { exception }, maxWaitTime = 5000)
            val e = exception
            if (e != null) {
                throw MiniCouchException("errss :$e", ExceptionEnum.GENERIC)
            }
            return qubit
        }
    }

    var isTrue by Table.isTrue
    var note by Table.note

    var actionType by ActionType referencedOn Table.actionType
    var account by User referencedOn Table.account

}