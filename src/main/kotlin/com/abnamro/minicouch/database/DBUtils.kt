package com.abnamro.minicouch.database

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.Column


object DBUtils {

    private val SHORT = ".{0,5}".toRegex()
    private val MEDIUM = ".{0,25}".toRegex()
    /**
     * This sql code can be added within a Table class by overwriting the createStatement() function
     * @view IntIdTable
     *
     */
    fun mutualExclusiveTrigger(a: Column<EntityID<Int>?>, b: Column<EntityID<Int>?>, triggerOn: TriggerOn): String {
        val tName = a.table.tableName
        val aName = a.name
        val bName = b.name

        val shortTableName = a.table.tableName
        val shortA = SHORT.find(aName)?.value
        val shortB = SHORT.find(aName)?.value
        val mediumTableName = MEDIUM.find(tName)?.value

        val name = """excl $shortTableName $shortA & $shortB B ${triggerOn.event.first()}"""
        return """
                    CREATE TRIGGER `$name`
                    BEFORE ${triggerOn.event}
                    ON `${a.table.tableName}`
                    FOR EACH ROW BEGIN
                    IF (        (NEW.$aName IS NULL AND NEW.$bName IS NULL )
                             OR
                                (NEW.$aName IS NOT NULL AND NEW.$bName IS NOT NULL )
                    ) THEN
                        signal sqlstate '45000' set message_text =
                        'TRIGGER: $mediumTableName.Table has exclusive disjunction columns';
                    END IF;
                    END
            """.trimIndent()
    }

    enum class TriggerOn(val event: String) {
        UPDATE("UPDATE"), INSERT("INSERT")
    }
}