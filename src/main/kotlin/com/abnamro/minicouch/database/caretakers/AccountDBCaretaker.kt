package com.abnamro.minicouch.database.caretakers

import com.abnamro.minicouch.database.tables.*
import com.abnamro.minicouch.services.ExceptionEnum
import com.abnamro.minicouch.services.MiniCouchException
import com.abnamro.minicouch.services.authorisation.Token
import com.abnamro.minicouch.utils.IBAN
import org.jetbrains.exposed.sql.transactions.transaction

/**
 *Handles all Account related DB requests
 */
object AccountDBCaretaker : AbstractCaretaker() {
    private lateinit var ACTION_TYPE_OPEN_ACCOUNT: ActionType


    /**
     * If any of the database records are changed, this caretaker has to be re-initiated. Because it will only load the "lateinit var"  fields once!
     */
    fun init() {
        var exception: Exception? = null
        var success: Boolean? = null
        transaction {
            try {
                val atoa = ActionType.find { ActionType.Table.actionCode eq "OA" }.firstOrNull()
                if (atoa != null) {
                    ACTION_TYPE_OPEN_ACCOUNT = atoa
                    success = true
                } else {
                    throw MiniCouchException(ExceptionEnum.DATABASE_INIT_FAILED)
                }
            } catch (e: Exception) {
                exception = e
            }
        }
        waitForNotNull({ success }, { exception }, maxWaitTime = 5000)
        success = handleReturnOrException(success, MiniCouchException(ExceptionEnum.GENERIC), exception)
    }

    init {
        init()
    }


    /**
     * Opens an accountID for the user's accountID
     * @throws MiniCouchException
     *@return the newly opened accountID
     */
    fun openAccount(token: Token): Account {
        val user = UserDBCaretaker.getUserByID(token.payload.uid)
        val qubitReturn = QubitCaretaker.checkCredentialsAndQubits(ACTION_TYPE_OPEN_ACCOUNT, token, user)
        when (qubitReturn.state) {
            QubitCaretaker.QubitState.ACCESS -> {
                val iban = generateRandomIBAN()
                if (IBAN.validateIBAN(iban)) {
                    var account: Account? = null
                    var exception: Exception? = null
                    transaction {
                        try {
                            val a = Account.new { this.iban = iban }
                            UserAccountAccess.new { this.user = user; this.account = a }
                            AccountHolder.new {
                                this.account = a
                                if (user.isPersonOwned()) {
                                    this.person = user.ownerPerson
                                    this.company = null
                                } else {
                                    this.person = null
                                    this.company = user.ownerPersonCompany!!.company
                                }
                            }
                            account = a
                        } catch (e: Exception) {
                            exception = e
                        }
                    }
                    waitForNotNull({ account }, { exception })
                    return handleReturnOrException(account, MiniCouchException(ExceptionEnum.ACCOUNT_UNKNOWN_OPEN_ACCOUNT), exception)
                } else {
                    throw MiniCouchException("Unable to generate a valid Iban", ExceptionEnum.ACCOUNT_INVALID_IBAN)
                }
            }
            QubitCaretaker.QubitState.LOW_CREDENTIALS -> throw MiniCouchException(qubitReturn.message, ExceptionEnum.ACTION_LOW_CREDENTIALS)
            QubitCaretaker.QubitState.QUBIT_FALSE -> throw MiniCouchException(qubitReturn.message, ExceptionEnum.ACTION_BLOCKED)
        }
    }


    /**
     * 1. Creates a random IBAN
     * 2. Checks database for existing accountID with the generated IBAN
     *      a. If not existing, continues to 3
     *      b. If existing, repeats step 2 (Up to 10 times, then throws ExceptionEnum.ACCOUNT_CONFLICT_IBAN)
     * 3. Returns IBAN
     *
     * @return a valid and unused IBAN
     * @throws MiniCouchException "To Many Requests", chance is one in a hundred quintillion with 1 million records!
     * @view ExceptionEnum.ACCOUNT_CONFLICT_IBAN
     */
    private fun generateRandomIBAN(): String {
        //try to find a new random IBAN, chance is astronomical it will find 10 duplicate IBANs
        // about 1 in hundred quintillion if we have 1 million records in our database!
        checkExistingIBAN@ for (i in 0..10) {
            val generatedIBAN = IBAN.generateRandomIBAN()
            var iBANDoesNotExist: Boolean? = null
            var exception: Exception? = null
            transaction {
                try {
                    iBANDoesNotExist = Account.find { Account.Table.iban eq generatedIBAN }.firstOrNull() == null
                } catch (e: Exception) {
                    exception = e
                }
            }
            waitForNotNull({ iBANDoesNotExist }, { exception })
            if (handleReturnOrException(iBANDoesNotExist, MiniCouchException(ExceptionEnum.ACCOUNT_CONFLICT_IBAN), exception)) {
                return generatedIBAN
            } else {
                checkExistingIBAN@ continue
            }
        }
        throw MiniCouchException("Too many requests", ExceptionEnum.ACCOUNT_CONFLICT_IBAN)
    }


    fun getUserAccounts(user: User): List<Account> {
        var allAccounts: ArrayList<Account>? = null
        var exception: Exception? = null

        transaction {
            try {
                val userAccounts = ArrayList<UserAccountAccess>()
                UserAccountAccess.find { UserAccountAccess.Table.user eq user.id.value }.forEach {
                    userAccounts.add(it)
                }
                val accounts = ArrayList<Account>()
                userAccounts.forEach {
                    val a = Account.findById(it.account.id)
                    if (a != null) accounts.add(a) else println("Account == null")
                }
                allAccounts = accounts
            } catch (e: Exception) {
                exception = e
            }
        }
        waitForNotNull({ allAccounts }, { exception })
        return handleReturnOrException(allAccounts, MiniCouchException(ExceptionEnum.ACCOUNT_UNKNOWN_OPEN_ACCOUNT), exception)
    }
}

