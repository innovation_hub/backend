package com.abnamro.minicouch.database.caretakers

import com.abnamro.minicouch.services.ExceptionEnum
import com.abnamro.minicouch.services.MiniCouchException
import org.jetbrains.exposed.sql.Database

open class AbstractCaretaker {
    companion object {
        val TIMEOUT = 1000 // seconds
    }

    init {
        val url = "jdbc:mysql://127.0.0.1:3306/mini_couch"
        Database.connect(url, driver = "com.mysql.jdbc.Driver", user = "root")
    }
}


/**
 * waitFor notNull being notNull
 * @view TIMEOUT can only take this many mili seconds
 * @throws MiniCouchException When timeout
 */
fun waitForNotNull(vararg notNull: () -> Any?, maxWaitTime: Int = AbstractCaretaker.TIMEOUT) {
    val timeout = System.currentTimeMillis() + maxWaitTime
    while (!notNull.any { it() != null }) {
        if (System.currentTimeMillis() > timeout) {
            throw (MiniCouchException(ExceptionEnum.TIME_OUT))
        }
        Thread.sleep(10) //wait until finished
    }
}

/**
 * @param Nullable type in
 * @param NotNull type out
 * @param value checks for null
 * @param mcEx MiniCouch Exception if something goes wrong
 * @param exception = exception will be cast to mini couch exception
 * @throws MiniCouchException
 * @view MiniCouchException
 */
@Suppress("UNCHECKED_CAST")
fun <Nullable : Any?, NotNull : Any> handleReturnOrException(value: Nullable, mcEx: MiniCouchException, exception: Exception?): NotNull =
        when {
            value != null -> value as NotNull
            exception != null -> if (exception::class.java == MiniCouchException::class.java) throw exception as MiniCouchException else throw MiniCouchException(exception.toString(), mcEx.error)
            else -> throw mcEx
        }




