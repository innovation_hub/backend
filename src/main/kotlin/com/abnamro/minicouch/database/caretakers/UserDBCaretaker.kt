package com.abnamro.minicouch.database.caretakers

import com.abnamro.minicouch.database.tables.*
import com.abnamro.minicouch.database.tables.qubits.UserQubit
import com.abnamro.minicouch.services.ExceptionEnum
import com.abnamro.minicouch.services.MiniCouchException
import com.abnamro.minicouch.services.authorisation.Token
import com.abnamro.minicouch.utils.PasswordUtils
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.transactions.transaction

object UserDBCaretaker : AbstractCaretaker() {


    lateinit var credentialEmailPassword: CredentialType

    /**
     * If any of the database records are changed, this caretaker has to be re-initiated. Because it will only load the "lateinit var"  fields once!
     */
    fun init() {
        transaction {
            val cep = CredentialType.find { CredentialType.Table.type eq CredentialType.Table.CREDENTIAL_EMAIL_PASSWORD }.firstOrNull()
            if (cep != null) {
                credentialEmailPassword = cep
            } else {
                throw MiniCouchException(ExceptionEnum.DATABASE_INIT_FAILED)
            }
        }
    }

    init {
        init()
    }

    /**
     * Login using an email and password
     * @throws MiniCouchException
     * @return token
     * @view Token
     */
    fun login(email: String, password: String): Token {
        var token: Token? = null
        var exception: MiniCouchException? = null
        transaction {
            val credential = CredentialEmailPassword.find { CredentialEmailPassword.Table.email eq email }.firstOrNull()
            if (credential != null) {
                if (PasswordUtils.isExpectedPassword(password, credential.salt, credential.passwordHash)) {
                    val passwordUserCredentials: UserAccessibleCredential? =
                            UserAccessibleCredential.find { UserAccessibleCredential.Table.credentialType eq credentialEmailPassword.id.value }
                                    .find { it.credentialTableID == credential.id.value }
                    if (passwordUserCredentials != null) {
                        if (passwordUserCredentials.isOpen) {
                            token = Token.createUserToken(passwordUserCredentials.user, passwordUserCredentials)
                        } else {
                            exception = MiniCouchException(ExceptionEnum.AUTH_CREDENTIAL_BLOCKED)
                        }
                    } else {
                        exception = MiniCouchException("User Credential does not exist", ExceptionEnum.AUTH_UNKNOWN_OR_INVALID_CREDENTIALS)
                    }
                    return@transaction
                }
            }
            exception = MiniCouchException(ExceptionEnum.AUTH_UNKNOWN_OR_INVALID_CREDENTIALS)
        }
        waitForNotNull({ exception }, { token })
        return handleReturnOrException(token, MiniCouchException(ExceptionEnum.GENERIC), exception)
    }

    /**
     * Login using a token
     * @throws MiniCouchException
     * @return token
     * @view Token
     */
    fun tokenLogin(token: String): Token = Token.decodeAndValidateToken(token)

    /**
     * Retrieves User by it's physical ID
     * @return User
     * @view User
     */
    fun getUserByID(uid: Int): User {
        var user: User? = null
        var exception: MiniCouchException? = null
        transaction {
            user = User.findById(uid)
            if (user == null) {
                exception = MiniCouchException(ExceptionEnum.AUTH_UNKNOWN_USER)
            }
            return@transaction
        }
        waitForNotNull({ exception }, { user })
        return handleReturnOrException(user, MiniCouchException(ExceptionEnum.GENERIC), exception)
    }


    /**
     * Checks if a person is not already using this email
     * @return emailUnused
     */
    @Deprecated("EMAIL SHOULD NO LONGER BE UNIQUE FOR PERSONS!")
    private fun isEmailNotUsedByPerson(email: String): Boolean {
        var notFound: Boolean? = null
        var exception: Exception? = null
        transaction {
            try {
                notFound = Person.find { Person.Table.email eq email }.firstOrNull() == null
            } catch (e: Exception) {
                exception = e
            }
        }
        waitForNotNull({ notFound }, { exception })
        return handleReturnOrException(notFound, MiniCouchException(ExceptionEnum.GENERIC), exception)
    }

    /**
     * Registers the Person and create it's User
     *
     * @param email
     * @param firstName
     * @param initials
     * @param surname
     * @param password
     *
     * @throws MiniCouchException
     *
     * @return Person
     * @view Person
     * @view User
     */
    fun registerPersonAndUser(firstName: String, initials: String, surname: String, email: String, password: String): Person {
        if (isEmailNotUsedByPerson(email)) {
            var person: Person? = null
            var exception: Exception? = null
            transaction {
                try {
                    val p = Person.new {
                        this.email = email
                        this.firstName = firstName
                        this.initials = initials
                        this.surname = surname
                    }
                    val success = registerUserWithCredential(this, p, null, email, password)
                    if (success) {
                        person = p
                    }
                } catch (e: Exception) {
                    exception = e
                }
            }
            waitForNotNull({ person }, { exception })
            return handleReturnOrException(person, MiniCouchException(ExceptionEnum.AUTH_PERSON_INSERT_FAILED), exception)
        } else {
            throw MiniCouchException(ExceptionEnum.AUTH_EMAIL_USED)
        }
    }

    private fun registerUserWithCredential(t: Transaction,
                                           person: Person?, personCompany: PersonCompanyEmployment?,
                                           email: String, password: String): Boolean {

        var success: Boolean? = null
        var exception: Exception? = null
        t.also {
            try {
                val user = User.new {
                    this.ownerPerson = person
                    this.ownerPersonCompany = personCompany
                }
                val cep = CredentialEmailPassword.new {
                    this.email = email
                    this.salt = PasswordUtils.nextSalt
                    this.passwordHash = PasswordUtils.hash(password, this.salt)
                }

                val uac = UserAccessibleCredential.new {
                    this.user = user
                    this.credentialType = credentialEmailPassword
                    this.credentialTableID = cep.id.value

                    //TODO: MAKE THIS FALSE && SEND EMAIL FOR OPENING CREDENTIAL
                    this.isOpen = true
                }

                val qu = UserQubit.new {
                    this.actionType = QubitCaretaker.ACTION_TYPE_OPEN_ACCOUNT
                    this.isTrue = false
                    this.note = "Not enough person info"
                    this.user = user
                }


                success = true
            } catch (e: Exception) {
                exception = e
            }
        }
        waitForNotNull({ success }, { exception })
        return handleReturnOrException(success, MiniCouchException(ExceptionEnum.AUTH_PERSON_INSERT_FAILED), exception)
    }


    /**
     * Registers a Person without User
     * @return Person
     * @view Person
     */
    fun registerPersonWithoutUser(firstName: String, initials: String, surname: String, personalEmail: String): Person {
        if (isEmailNotUsedByPerson(personalEmail)) {
            var person: Person? = null
            var exception: Exception? = null
            transaction {
                try {
                    person = Person.new {
                        this.email = personalEmail
                        this.firstName = firstName
                        this.initials = initials
                        this.surname = surname
                    }
                } catch (e: Exception) {
                    exception = e
                }
            }
            waitForNotNull({ person }, { exception })
            return handleReturnOrException(person, MiniCouchException(ExceptionEnum.AUTH_PERSON_INSERT_FAILED), exception)
        } else {
            throw MiniCouchException(ExceptionEnum.AUTH_EMAIL_USED)
        }
    }

    /**
     * Registers "Company" User for a Person
     * @return Person Company Employment
     * @view PersonCompanyEmployment
     */
    fun registerCompanyPersonAndUser(person: Person, password: String, company: Company, companyPersonEmail: String): PersonCompanyEmployment {
        if (isEmailNotUsedByPerson(companyPersonEmail)) {
            var personCompanyE: PersonCompanyEmployment? = null
            var exception: Exception? = null
            transaction {
                try {
                    val pc = PersonCompanyEmployment.new {
                        this.company = company
                        this.person = person
                        this.employeeEmail = companyPersonEmail
                    }
                    if (registerUserWithCredential(this, null, pc, companyPersonEmail, password)) {
                        personCompanyE = pc
                    } else {
                        exception = MiniCouchException(ExceptionEnum.AUTH_PC_EMPLOYMENT_INSERT_FAILED)
                    }
                } catch (e: Exception) {
                    exception = e
                }
            }
            waitForNotNull({ personCompanyE }, { exception })
            return handleReturnOrException(personCompanyE, MiniCouchException(ExceptionEnum.AUTH_PC_EMPLOYMENT_INSERT_FAILED), exception)
        } else {
            throw MiniCouchException(ExceptionEnum.AUTH_EMAIL_USED)
        }
    }


}



