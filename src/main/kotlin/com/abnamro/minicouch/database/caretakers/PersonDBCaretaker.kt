package com.abnamro.minicouch.database.caretakers

import com.abnamro.minicouch.database.tables.Person
import com.abnamro.minicouch.database.tables.User
import com.abnamro.minicouch.database.tables.qubits.UserQubit
import com.abnamro.minicouch.services.ExceptionEnum
import com.abnamro.minicouch.services.MiniCouchException
import com.abnamro.minicouch.services.authorisation.Token
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import org.joda.time.DateTime

object PersonDBCaretaker : AbstractCaretaker() {
    /**
     * Updates all non null fields in Person
     *
     * @param user
     * @param firstName
     * @param initials
     * @param surname
     * @param email
     * @param birthDate
     * @param birthPlace
     *
     */
    fun updatePersonInformation(user: User, firstName: String? = null, initials: String? = null, surname: String? = null, email: String? = null, birthDate: String?, birthPlace: String?): Person {
        var exception: Exception? = null
        var success: Boolean? = null
        var notUpdatedPerson: Person? = null
        var person: Person? = null
        transaction {
            try {
                notUpdatedPerson = user.getPerson()
                Person.Table.update({ Person.Table.id eq notUpdatedPerson!!.id.value }) {
                    if (firstName != null)
                        it[this.firstName] = firstName
                    if (initials != null)
                        it[this.initials] = initials
                    if (surname != null)
                        it[this.surname] = surname
                    if (email != null)
                        it[this.email] = email
                    if (birthDate != null)
                        it[this.birthDate] = DateTime.parse(birthDate)
                    if (birthPlace != null)
                        it[this.birthPlace] = birthPlace
                    success = true
                }
            } catch (ex: IllegalArgumentException) {
                exception = MiniCouchException(ExceptionEnum.PERSON_INVALID_DATE)
            } catch (e: Exception) {
                exception = e
            }
        }
        waitForNotNull({ success }, { exception })
        if (handleReturnOrException(success, MiniCouchException(ExceptionEnum.PERSON_UPDATE_FAILED), exception))
            transaction {
                person = Person.findById(notUpdatedPerson!!.id)
            }
        waitForNotNull({ person }, { exception })

        //reset Qubits for all person's users
        person?.getAllUsers()?.forEach { UserQubit.resetQubits(it) }

        return handleReturnOrException(person, MiniCouchException(ExceptionEnum.PERSON_NOT_FOUND), exception)
    }

    /**
     * @param token User's access token
     * @return person from user from token
     * @view UserDBCaretaker
     * @throws MiniCouchException PERSON_NOT_FOUND
     */
    fun retrievePersonFromToken(token: Token): Person {
        var person: Person? = null
        var exception: Exception? = null
        transaction {
            try {
                person = UserDBCaretaker.getUserByID(token.payload.uid).getPerson()
            } catch (e: Exception) {
                exception = e
            }
        }
        waitForNotNull({ person }, { exception })
        return handleReturnOrException(person, MiniCouchException(ExceptionEnum.PERSON_NOT_FOUND), exception)
    }
}