package com.abnamro.minicouch.database.caretakers

import com.abnamro.minicouch.database.tables.*
import com.abnamro.minicouch.database.tables.qubits.AccountQubit
import com.abnamro.minicouch.database.tables.qubits.UserQubit
import com.abnamro.minicouch.services.ExceptionEnum
import com.abnamro.minicouch.services.MiniCouchException
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction


//fun main(args: Array<String>) {
//    println("MAIN RUN InitDBCaretaker")
//    println("Init DB: ${InitDBCaretaker.initDatabase()}")
//
//     InitDBCaretaker.testPersonInit("Albert")
//    // InitDBCaretaker.testCompanyPersonInit()
//    // InitDBCaretaker.testPersonInit("Dennis")
//}


object InitDBCaretaker : AbstractCaretaker() {

    /**
     * @throws MiniCouchException
     */
    fun initDatabase(): Boolean {
        var success: Boolean? = null
        var exception: Exception? = null
        transaction {
            try {
                //drops all (old) triggers
                exec("""SELECT Concat('DROP TRIGGER ', Trigger_Name, ';') FROM  information_schema.TRIGGERS WHERE TRIGGER_SCHEMA = 'your_schema';""")
                //Dropping all tables
                //Disable foreign key checkCredAndQubit
                exec("SET FOREIGN_KEY_CHECKS = 0")
                //query to return al tables present in the 'mini_couch' database
                exec("""
                     SELECT concat('DROP TABLE IF EXISTS ', table_name, ';')
                     FROM information_schema.tables
                     WHERE table_schema = 'mini_couch';
                """.trimIndent()) {
                    while (it.next()) {
                        exec(it.getString(1))
                    }
                }
                //enable foreign key checkCredAndQubit
                exec("SET FOREIGN_KEY_CHECKS = 1")

                //create
                //Step 1
                SchemaUtils.create(Account.table)
                SchemaUtils.create(Company.table)
                SchemaUtils.create(Person.table)
                SchemaUtils.create(CredentialType.table)
                SchemaUtils.create(ActionType.table)
                //Step 2
                SchemaUtils.create(PersonCompanyEmployment.table)
                //Step 3
                SchemaUtils.create(User.table)
                //Step 4
                SchemaUtils.create(UserAccessibleCredential.table)
                SchemaUtils.create(UserAccountAccess.table)
                SchemaUtils.create(AccountHolder.table)
                //Step 5
                SchemaUtils.create(CredentialEmailPassword.table)

                //Step 6 QUBITS
                SchemaUtils.create(UserQubit.table)
                SchemaUtils.create(AccountQubit.table)
                success = true
            } catch (e: Exception) {
                exception = e
            }
        }
        waitForNotNull({ success }, { exception }, maxWaitTime = 5000)
        if (success != null) {
            AccountDBCaretaker.init()
            QubitCaretaker.init()
            UserDBCaretaker.init()

            return success as Boolean
        } else {
            val e = exception
            if (e != null) {
                throw MiniCouchException(e.toString(), ExceptionEnum.DATABASE_INIT_FAILED)
            } else {
                throw MiniCouchException(ExceptionEnum.DATABASE_INIT_FAILED)
            }
        }
    }

    /**
     * creates a test user with account
     */
    fun testPersonInit(name: String) {
        val email = "$name@test.com"
        val password = "123456"
        UserDBCaretaker.registerPersonAndUser("$name", "B", "Cent", email, password)
        val token = UserDBCaretaker.login(email, password)
        AccountDBCaretaker.openAccount(token)
    }

    /**
     * Creater a few Persons with user and without
     * couples them to company
     *
     * and create a company account
     */
    fun testCompanyPersonInit() {
        val companyEmail = "B@TestComp.nl"
        val password = "123456"

        var company: Company? = null
        transaction {
            val c = Company.new {
                this.name = "Test Company"
            }
            val person: Person = UserDBCaretaker.registerPersonWithoutUser("Bert", "", "Krent", "B@test.com")
            UserDBCaretaker.registerCompanyPersonAndUser(person, password, c, companyEmail)
            company = c
        }
        waitForNotNull({ company })
        val person2 = UserDBCaretaker.registerPersonAndUser("Cweetikveel", "H H", "Geenidee", "C@test.nl", password)
        UserDBCaretaker.registerCompanyPersonAndUser(person2, password, company!!, "C@TestComp.nl")


        val token = UserDBCaretaker.login(companyEmail, password)
        AccountDBCaretaker.openAccount(token)
    }
}

