package com.abnamro.minicouch.database.caretakers

import com.abnamro.minicouch.database.tables.ActionType
import com.abnamro.minicouch.database.tables.User
import com.abnamro.minicouch.services.ExceptionEnum
import com.abnamro.minicouch.services.MiniCouchException
import com.abnamro.minicouch.services.authorisation.Token
import org.jetbrains.exposed.sql.transactions.transaction

object QubitCaretaker : AbstractCaretaker() {

    lateinit var ACTION_TYPE_OPEN_ACCOUNT: ActionType
    lateinit var ACTION_TYPE_VIEW_PERSON: ActionType
    lateinit var ACTION_TYPE_CHANGE_PERSON: ActionType


    /**
     * If any of the database records are changed, this caretaker has to be re-initiated. Because it will only load the "lateinit var"  fields once!
     */
    fun init() {
        var exception: Exception? = null
        var success: Boolean? = null
        transaction {
            try {
                ACTION_TYPE_OPEN_ACCOUNT = ActionType.find { ActionType.Table.actionCode eq "OA" }.first()
                ACTION_TYPE_VIEW_PERSON = ActionType.find { ActionType.Table.actionCode eq "VP" }.first()
                ACTION_TYPE_CHANGE_PERSON = ActionType.find { ActionType.Table.actionCode eq "CP" }.first()
                success = true
            } catch (e: Exception) {
                exception = e
            }
        }
        waitForNotNull({ success }, { exception })
        success = handleReturnOrException(success, MiniCouchException(ExceptionEnum.DATABASE_INIT_FAILED), exception)
    }

    init {
        init()
    }

    data class QubitReturn(val state: QubitState, val message: String = "")

    enum class QubitState {
        ACCESS,
        LOW_CREDENTIALS,
        QUBIT_FALSE
    }

    /**
     * Do a full checkCredAndQubit
     */
    fun checkCredentialsAndQubits(actionType: ActionType, token: Token, u: User?): QubitReturn {
        val user: User = u ?: UserDBCaretaker.getUserByID(token.payload.uid)
        return if (actionType.checkMinimumCredentials(token.payload.credentials)) {
            val errorMessage = actionType.checkQubits(user)
            if (errorMessage == null) {
                QubitReturn(QubitState.ACCESS)
            } else {
                QubitReturn(QubitState.QUBIT_FALSE, errorMessage)
            }
        } else {
            QubitReturn(QubitState.LOW_CREDENTIALS, "low credentials")
        }
    }
}