package com.abnamro.minicouch.services

import java.util.logging.Logger

// extend any class with the ability to get a logger
fun <T : Any> T.logger(): Logger {
    val l =  Logger.getLogger(this::class.java.name)

    return l
}