package com.abnamro.minicouch.services.temp

import com.abnamro.minicouch.services.ExceptionEnum
import com.abnamro.minicouch.services.ExceptionResponse
import com.abnamro.minicouch.services.MiniCouchException
import org.springframework.web.bind.annotation.*
import java.util.concurrent.atomic.AtomicLong
import javax.servlet.http.HttpServletRequest

class DummyModel(val id: Long, val content: String)

@CrossOrigin //https://stackoverflow.com/questions/35672679/add-access-control-allow-origin-to-web-service
@RestController
class DummyPostController {
    val counter = AtomicLong()
    @RequestMapping(value = "/dummypost", method = arrayOf(RequestMethod.POST))
    fun dummyPost(
            @RequestParam(value = "param1", defaultValue = "Default First") firstName: String,
            @RequestParam(value = "param2", defaultValue = "Default Last") lastName: String,
            @RequestParam(value = "throw_error", defaultValue = "0") throwError: Int) =
            when (throwError) {
                1 -> throw Exception("Test Dummy Exception")
                2 -> throw MiniCouchException("Test Exception", ExceptionEnum.GENERIC)
                else -> DummyModel(counter.incrementAndGet(), "Hello Post $firstName $lastName")
            }
}

@CrossOrigin //https://stackoverflow.com/questions/35672679/add-access-control-allow-origin-to-web-service
@RestController
class DummyController {

    val counter = AtomicLong()
    @RequestMapping(value = "/dummy", method = arrayOf(RequestMethod.GET))
    fun greeting(
            @RequestParam(value = "param1", defaultValue = "Default First") firstName: String,
            @RequestParam(value = "param2", defaultValue = "Default Last") lastName: String,
            @RequestParam(value = "throw_error", defaultValue = "0") throwError: Int
    ): DummyModel {
        when (throwError) {
            1 -> throw Exception("Test Dummy Exception")
            2 -> throw MiniCouchException("Test Exception", ExceptionEnum.GENERIC)
        }
        return DummyModel(counter.incrementAndGet(), "Hello $firstName $lastName")
    }

    @ExceptionHandler(Exception::class)
    fun randomException(request: HttpServletRequest, ex: Exception): ExceptionResponse {
        return ExceptionResponse(request, ex)
    }

    @ExceptionHandler(MiniCouchException::class)
    fun randomException(request: HttpServletRequest, ex: MiniCouchException): ExceptionResponse {
        return ExceptionResponse(request, ex)
    }
}
