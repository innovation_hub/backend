package com.abnamro.minicouch.services.temp

import com.abnamro.minicouch.database.caretakers.InitDBCaretaker
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView

@CrossOrigin //https://stackoverflow.com/questions/35672679/add-access-control-allow-origin-to-web-service
@RestController
class InitController {
    @RequestMapping(value = "/temp", method = arrayOf(RequestMethod.GET))
    @Throws(Exception::class)
    fun login(
            @RequestParam(value = "secret", defaultValue = "") secret: String): Boolean {
        return if (secret == "lol") {
            InitDBCaretaker.initDatabase()

        } else {
            false
        }
    }

    @ExceptionHandler(Exception::class)
    fun handleAllException(ex: Exception): ModelAndView {
        val model = ModelAndView("/error")
        model.addObject("errMsg", "this is Exception.class")
        return model
    }
}