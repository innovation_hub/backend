package com.abnamro.minicouch.services

/**
 * Use to return responses
 */
class ResponseObject<out DATA>(val data: DATA) {
    val code = 200
    val timestamp = System.currentTimeMillis()
}