package com.abnamro.minicouch.services.accounts

import com.abnamro.minicouch.database.caretakers.AccountDBCaretaker
import com.abnamro.minicouch.database.caretakers.UserDBCaretaker
import com.abnamro.minicouch.services.*
import com.abnamro.minicouch.services.authorisation.Token
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest

/**
 * Can't return Account, because of how the JSON is created
 */
class SimpleAccount(val id: Int, val iban: String)


/**
 * Test out with postman. (Check Bitbucket readme for URL)
 * https://bitbucket.org/innovation_hub/backend
 *
 * Step: 3. Account - Open Account
 * Step: 4. Account - Retrieve Account

 *
 */
@RestController
@CrossOrigin //https://stackoverflow.com/questions/35672679/add-access-control-allow-origin-to-web-service
class OpenAccountController {
    @RequestMapping(value = "/open_account", method = arrayOf(RequestMethod.POST))
    fun openAccount(@RequestParam(value = "token", defaultValue = "") token: String): ResponseObject<SimpleAccount> {
        val account = AccountDBCaretaker.openAccount(Token.decodeAndValidateToken(token))
        return ResponseObject(SimpleAccount(account.id.value, account.iban))
    }

    @RequestMapping(value = "/view_user_accounts", method = arrayOf(RequestMethod.GET))
    fun viewAccounts(@RequestParam(value = "token", defaultValue = "") token: String): ResponseObject<List<SimpleAccount>> {
        val list = ArrayList<SimpleAccount>()
        AccountDBCaretaker.getUserAccounts(
                UserDBCaretaker.getUserByID(Token.decodeAndValidateToken(token).payload.uid)
        ).forEach {
            list.add(SimpleAccount(it.id.value, it.iban))
        }
        val l = list.toList()
        if (l.isEmpty()) {
            throw MiniCouchException(ExceptionEnum.ACCOUNT_NO_REGISTERED_ACCOUNTS)
        } else {
            return ResponseObject(l)
        }
    }

    @ExceptionHandler(MiniCouchException::class)
    fun handleAuthException(request: HttpServletRequest, ex: MiniCouchException): ExceptionResponse {
        return ExceptionResponse(
                code = ex.error.code,
                status = ex.error.httpResponseCode,
                statusMessage = ex.responseCodeMessage,
                response = ex.miniCouchCodeMessage,
                ex = ex,
                path = request.servletPath)
    }
}

