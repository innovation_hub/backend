package com.abnamro.minicouch.services

import javax.servlet.http.HttpServletRequest

/**
 * Our own MiniCouchException!
 * When this one gets thrown you know we are catching an unhappy flow somewhere in the program.
 *
 * @param responseCodeMessage Optionally it's possible to change the message of the exception
 * @param error Select the error/exception, if you can not find this, create a new one or search better.

 * @view ExceptionEnum Right click the correct enum which gets thrown
 * @view ExceptionResponse can be returned to the user using this model
 */
class MiniCouchException(val responseCodeMessage: String, val miniCouchCodeMessage: String, val error: ExceptionEnum) : Exception(responseCodeMessage) {
    constructor(code: ExceptionEnum) : this(code.responseCodeMessage, code.miniCouchMessage, code)
    constructor(miniCouchMessage: String, code: ExceptionEnum) : this(code.responseCodeMessage, miniCouchMessage, code)

}

/**
 * Look at Google drive:
 * https://docs.google.com/spreadsheets/d/1Q3PDXXrSl8Tr6v0_ZAsZm9112OxYCouzEdaRnAyIq_E/edit#gid=0
 * It contains all the exceptions.
 * When creating a new one, insert it in the excel sheet, than copy the generated Kotlin code!
 *
 * Don't forget to auto-format after pasting it below!
 *
 * @view MiniCouchException
 */
enum class ExceptionEnum(val code: Int, val httpResponseCode: Int, val responseCodeMessage: String, val miniCouchMessage: String) {
    UNKNOWN(-1, 520, "UNKNOWN Exception", "?"),
    GENERIC(0, 520, "Generic Exception", "?"),
    TIME_OUT(1, 408, "Request Timeout", "Request Timeout"),
    AUTH_INVALID_FORMAT(2, 498, "Invalid Token (Esri)", "Format issue"),
    AUTH_HASH_TEMPERING(3, 498, "Invalid Token (Esri)", "Hash tempering"),
    AUTH_INVALID_JSON(4, 498, "Invalid Token (Esri)", "Json parse error"),
    AUTH_EXPIRED_TOKEN(5, 498, "Invalid Token (Esri)", "Token has expired"),
    AUTH_PASSWORD_CHANGED(6, 498, "Invalid Token (Esri)", "Password has been changed"),
    AUTH_MISSING_TOKEN(7, 422, "Unprocessable Entity", "No token supplied"),
    AUTH_MISSING_CREDENTIALS(8, 422, "Unprocessable Entity", "Missing user&/password"), //https://www.bennadel.com/blog/2434-http-status-codes-for-invalid-data-400-vs-422.htm
    AUTH_UNKNOWN_OR_INVALID_CREDENTIALS(9, 401, "Unauthorized", "Unknown User &/ password combination"),
    AUTH_UNKNOWN_USER(10, 401, "Unauthorized", "Unknown User &/ password combination"),
    AUTH_EMAIL_USED(11, 409, "Conflict", "Email address already in use"),//https://stackoverflow.com/questions/3825990/http-response-code-for-post-when-resource-already-exists
    AUTH_PERSON_INSERT_FAILED(12, 500, "Internal Server Error", "Failed to create Person"),
    AUTH_PC_EMPLOYMENT_INSERT_FAILED(13, 500, "Internal Server Error", "Failed to create Employment for Person and Compay"),
    AUTH_CREDENTIAL_BLOCKED(14, 401, "Unauthorized", "User credential is blocked"),
    ACCOUNT_UNKNOWN_OPEN_ACCOUNT(15, 520, "Unknown Exception", "While opening accountID"),
    ACCOUNT_UNKNOWN_GET_ACCOUNTS(16, 520, "Unknown Exception", "While retrieving account"),
    ACCOUNT_CONFLICT_IBAN(17, 409, "Conflict", "New IBAN can not be generated"),
    ACCOUNT_INVALID_IBAN(18, 422, "Unprocessable Entity", "IBAN is invalid"),
    ACCOUNT_CONFLICT(19, 500, "Internal Server Error", "While opening accountID"),
    ACCOUNT_NO_REGISTERED_ACCOUNTS(20, 204, "No Content", "No accounts registered for this user"),
    PERSON_INVALID_DATE(21, 422, "Unprocessable Entity", "Unprocessable date"),
    PERSON_UPDATE_FAILED(22, 520, "Unknown Exception", "Unable to update person"),
    PERSON_NOT_FOUND(23, 520, "Unknown Exception", "Unable to find person"),
    DATABASE_INIT_FAILED(24, 500, "Internal Server Error", "Database could not be initialised"),
    DATABASE_RETRIEVE_FAILED(25, 500, "Internal Server Error", "Database record could not be retrieved"),
    ACTION_BLOCKED(26, 401, "Unauthorized", "Database record could not be retrieved"),
    ACTION_LOW_CREDENTIALS(27, 401, "Unauthorized", "This action requires a higher credential rating"),
    QUANTUM_BOOLEAN_GENERAL(28, 401, "Unauthorized", "This action is not allowed"),
    QUANTUM_BOOLEAN_OPEN_ACCOUNT(29, 401, "Unauthorized", "User is not allowed to open an account"),
    QUANTUM_BOOLEAN_COLLECT_NOTES(30, 401, "Unauthorized", "This action is blocked for unknown reasons"),
}

/**
 * Used to return Exceptions to the user
 * @param code miniCouch error code
 * @param status http status code
 * @param statusMessage http status message
 * @param response mini couch error message
 * @param ex exception
 * @param path path of start exception
 * *
 * @view Exception
 * @view MiniCouchException
 */
class ExceptionResponse(val code: Int,
                        val status: Int = 520,
                        val statusMessage: String = "Unknown Error",
                        val response: String?, ex: Exception,
                        val path: String) {
    constructor(request: HttpServletRequest,
                ex: MiniCouchException) :
            this(code = ex.error.code,
                    status = ex.error.httpResponseCode,
                    statusMessage = ex.responseCodeMessage,
                    response = ex.miniCouchCodeMessage,
                    ex = ex, path = request.servletPath){
        logger().warning("constructor: ${ex.error}")
    }

    /**
     * Not caught Exceptions
     */
    constructor(request: HttpServletRequest,
                ex: Exception) :
            this(-1,
                    status = 520,
                    statusMessage = "Unknown Exception",
                    response = ex.message,
                    ex = ex,
                    path = request.servletPath)


    val exception = ex::class.java
    val timestamp = System.currentTimeMillis()

    private //remove for debug stacktrace etc
    val debug = ex

}