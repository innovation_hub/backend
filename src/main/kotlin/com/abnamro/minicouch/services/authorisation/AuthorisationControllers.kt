package com.abnamro.minicouch.services.authorisation

import com.abnamro.minicouch.database.caretakers.UserDBCaretaker
import com.abnamro.minicouch.database.tables.SimplePerson
import com.abnamro.minicouch.services.ExceptionEnum
import com.abnamro.minicouch.services.ExceptionResponse
import com.abnamro.minicouch.services.MiniCouchException
import com.abnamro.minicouch.services.ResponseObject
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest


@RestController
@CrossOrigin //https://stackoverflow.com/questions/35672679/add-access-control-allow-origin-to-web-service
class LoginController {
    @RequestMapping(value = "/login", method = arrayOf(RequestMethod.POST))
    @Throws(Exception::class)
    fun login(
            @RequestParam(value = "email", defaultValue = "") email: String,
            @RequestParam(value = "password", defaultValue = "") password: String,
            @RequestParam(value = "token", defaultValue = "") token: String
    ): ResponseObject<String> {
        val t: Token =
                if (token.isNotEmpty()) {
                    UserDBCaretaker.tokenLogin(token)
                } else {
                    if (email.isNotEmpty() && password.isNotEmpty()) {
                        UserDBCaretaker.login(email, password)
                    } else {
                        throw(MiniCouchException(ExceptionEnum.AUTH_MISSING_CREDENTIALS))
                    }
                }
        return ResponseObject(t.jwt())
    }


    @ExceptionHandler(MiniCouchException::class)
    fun handleAuthException(request: HttpServletRequest, ex: MiniCouchException): ExceptionResponse {
        return ExceptionResponse(request, ex)
    }
}


data class SimplePersonRegisterResponse(val registerMessage: String, val person: SimplePerson)

@RestController
@CrossOrigin
class RegisterController {
    @RequestMapping(value = "/register", method = arrayOf(RequestMethod.POST))
    @Throws(Exception::class)
    fun create(
            @RequestParam(value = "first_name", defaultValue = "") name: String,
            @RequestParam(value = "initials", defaultValue = "") initials: String,
            @RequestParam(value = "surname", defaultValue = "") surname: String,
            @RequestParam(value = "email", defaultValue = "") email: String,
            @RequestParam(value = "password", defaultValue = "") password: String
    ): ResponseObject<SimplePersonRegisterResponse> {

        if (name.isNotEmpty() &&
                surname.isNotEmpty() &&
                email.isNotEmpty() &&
                password.isNotEmpty()) {
            val person = UserDBCaretaker.registerPersonAndUser(name, initials, surname, email, password)
            return ResponseObject(SimplePersonRegisterResponse("Registered, Email will be send after JP gets a beard :-)", SimplePerson.init(person)))
        } else {
            throw(MiniCouchException(ExceptionEnum.AUTH_MISSING_CREDENTIALS))
        }
    }

    @ExceptionHandler(MiniCouchException::class)
    fun handleAuthException(request: HttpServletRequest, ex: MiniCouchException): ExceptionResponse {
        return ExceptionResponse(request, ex)
    }
}