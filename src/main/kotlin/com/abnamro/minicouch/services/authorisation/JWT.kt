package com.abnamro.minicouch.services.authorisation

import com.abnamro.minicouch.database.tables.User
import com.abnamro.minicouch.database.tables.UserAccessibleCredential
import com.abnamro.minicouch.services.ExceptionEnum
import com.abnamro.minicouch.services.MiniCouchException
import com.squareup.moshi.JsonDataException
import com.squareup.moshi.Moshi
import com.sun.org.apache.xerces.internal.impl.dv.util.HexBin
import org.joda.time.Hours
import java.util.*
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

/**
 * Utils object, holds mostly some functions and default values
 * @view Token
 */
object JWT {
    /**
     * used for JSONs
     */
    val MOSHI: Moshi = Moshi.Builder().build()
    private val ALGORITHM = "HmacSHA256"
    /**
     * Secret Key is secret
     * TODO: Place this somewhere else. Don't ask me where, you figure it out please :-)
     */
    val KEY = "secret"

    /**
     * Expiration time of the token, standard 1 hour
     */
    val TOKEN_EXPIRATION = Hours.ONE.toStandardMinutes().toStandardSeconds().seconds


    /**
     * Encodes with Base64
     */
    fun base64UrlEncode(data: String) = Base64.getEncoder().encodeToString(data.toByteArray())

    /**
     * Decodes URL with Base64
     */
    fun base64UrlDecode(data: String) = String(Base64.getUrlDecoder().decode(data))

    /**
     * HASH with secret and encode
     * @return signature
     */
    fun hashWithSecretSalt(data: String, secretSaltValue: String): String {
        val secretSalt = SecretKeySpec(secretSaltValue.toByteArray(), JWT.ALGORITHM)
        val sha256HMAC = Mac.getInstance(JWT.ALGORITHM)
        sha256HMAC.init(secretSalt)
        return HexBin.encode(sha256HMAC.doFinal(data.toByteArray()))
    }
}


/**
 * Info on JWT token: https://auth0.com/docs/jwt
 * @param header
 * @param payload
 */
class Token(val header: Header = Header(), val payload: Payload) {

    companion object {
        /**
         * Checks for empty token, decodes, authenticates and Validates JWT token.
         *
         * @throws MiniCouchException
         * @view Token
         * @return Token
         */
        fun decodeAndValidateToken(jwt: String): Token {
            if (jwt.isEmpty()) {
                throw MiniCouchException(ExceptionEnum.AUTH_MISSING_CREDENTIALS)
            }
            val hps = jwt.split(".")
            if (hps.size != 3) {
                throw MiniCouchException(ExceptionEnum.AUTH_INVALID_FORMAT)
            }
            val encodedHeader = hps[0]
            val encodedPayload = hps[1]
            val encodedSignature = hps[2]

            val jsonHeader = JWT.base64UrlDecode(encodedHeader)
            val jsonPayload = JWT.base64UrlDecode(encodedPayload)
            val signature = JWT.base64UrlDecode(encodedSignature)

            val rehashSignature = JWT.hashWithSecretSalt("$encodedHeader.$encodedPayload", JWT.KEY)
            if (rehashSignature == signature) {
                try {
                    val headerObject = JWT.MOSHI.adapter<Header>(Header::class.java).fromJson(jsonHeader) as Header
                    val payloadObject = JWT.MOSHI.adapter<Payload>(Payload::class.java).fromJson(jsonPayload) as Payload
                    if (payloadObject.isNotExpired()) {


                        //TODO: FIX THIS
                        return Token(headerObject, payloadObject)
                        //TODO: FIX THIS
                        //val user = UserDBCaretaker.getUserByID(payloadObject.uid)
                        // if (user.passwordUpdated.millis / 1000 < payloadObject.iat) {
                        //     return Token(headerObject, payloadObject)
                        // } else {
                        //     throw MiniCouchException(ExceptionEnum.AUTH_PASSWORD_CHANGED)
                        // }


                    } else {
                        throw MiniCouchException(ExceptionEnum.AUTH_EXPIRED_TOKEN)
                    }
                } catch (exception: JsonDataException) {
                    throw MiniCouchException(ExceptionEnum.AUTH_INVALID_JSON)
                }
            } else {
                throw MiniCouchException(ExceptionEnum.AUTH_HASH_TEMPERING)
            }
        }

        fun createUserToken(user: User, passwordUserCredentials: UserAccessibleCredential): Token {
            val currentTime = System.currentTimeMillis() / 1000
            return Token(Header(), Payload(
                    sub = "Authorisation",
                    iss = "Android / Web",
                    uid = user.id.value,
                    iat = currentTime,
                    exp = currentTime + JWT.TOKEN_EXPIRATION,
                    name = user.getPerson().fullName(),
                    credentials = listOf(SimpleCredential.from(passwordUserCredentials)) //TODO FIX THIS
            ))
        }

        fun renewUserToken(old: Token): Token {
            val currentTime = System.currentTimeMillis() / 1000
            return Token(old.header, Payload(
                    sub = old.payload.sub,
                    iss = old.payload.iss,
                    uid = old.payload.uid,
                    iat = currentTime,
                    exp = currentTime + JWT.TOKEN_EXPIRATION,
                    name = old.payload.name,
                    credentials = old.payload.credentials
            ))
        }
    }

    //JSON STRINGS
    @Transient private val jsonHeader = JWT.MOSHI.adapter<Header>(Header::class.java).toJson(header)
    @Transient private val jsonPayload = JWT.MOSHI.adapter<Payload>(Payload::class.java).toJson(payload)

    //BASE64 ENCODED JSON STRINGS

    @Transient private val headerEncoded = JWT.base64UrlEncode(jsonHeader)
    @Transient private val payloadEncoded = JWT.base64UrlEncode(jsonPayload)

    //HASHED JSON STRINGS
    @Transient private val signature = JWT.hashWithSecretSalt("$headerEncoded.$payloadEncoded", JWT.KEY)

    //BASE64 ENCODED JSON STRINGS
    @Transient private val signatureEncoded = JWT.base64UrlEncode(signature)

    /**
     * Returns the Token as a JWT String
     */
    fun jwt(): String = "$headerEncoded.$payloadEncoded.$signatureEncoded"

    /**
     * Returns the Token as a JSONFormat
     */
    fun jsonJwt(): String = JWT.MOSHI.adapter<Token>(Token::class.java).toJson(this)
}


/**
 * @param alg Algorithm default: HS256
 * @param typ Type default JWT
 * @view Token
 */
class Header(val alg: String = "HS256", val typ: String = "JWT") {
    override fun toString(): String {
        return "Header(alg='$alg', typ='$typ')"
    }
}

/**
 * @param iss Issuer
 * @param uid User ID
 * @param iat Issued At
 * @param exp Expiration Time
 * @param sub Subject
 * @param name Name
 * @view Token
 */
class Payload(val iss: String,
              val uid: Int,
              val iat: Long,
              val exp: Long,
              val sub: String,
              val name: String,
              val credentials: List<SimpleCredential>) {


    fun isNotExpired(): Boolean {
        return (System.currentTimeMillis() / 1000) < exp
    }

    override fun toString(): String {
        return "Payload(iss='$iss', uid=$uid, iat=$iat, exp=$exp, sub='$sub', name='$name')"
    }
}

/**
 * Use this to save the (e.g. login-) credentials of the user in the token.
 * @view Token
 */
class SimpleCredential(val type: String, val level: Int) {
    companion object {
        fun from(userCredential: UserAccessibleCredential) =
                SimpleCredential(userCredential.credentialType.type, userCredential.credentialType.rating)
    }
}






