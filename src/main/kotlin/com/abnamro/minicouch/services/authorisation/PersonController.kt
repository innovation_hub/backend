package com.abnamro.minicouch.services.authorisation

import com.abnamro.minicouch.database.caretakers.PersonDBCaretaker
import com.abnamro.minicouch.database.caretakers.UserDBCaretaker
import com.abnamro.minicouch.database.tables.SimplePerson
import com.abnamro.minicouch.services.*
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest

@RestController
@CrossOrigin
class PersonController {
    @RequestMapping(value = "/update_person", method = arrayOf(RequestMethod.POST))
    @Throws(MiniCouchException::class)
    fun update(
            @RequestParam(value = "token", defaultValue = "", required = true) t: String,
            @RequestParam(value = "first_name", required = false) name: String?,
            @RequestParam(value = "initials", required = false) initials: String?,
            @RequestParam(value = "surname", required = false) surname: String?,
            @RequestParam(value = "email", required = false) email: String?,
            @RequestParam(value = "birth_date", required = false) birthDate: String?,
            @RequestParam(value = "birth_place", required = false) birthPlace: String?
    ): ResponseObject<SimplePerson> {
        if (arrayOf(name, initials, surname, email, birthDate, birthPlace).any { it != null }) {
            val token = Token.decodeAndValidateToken(t)
            val user = UserDBCaretaker.getUserByID(token.payload.uid)
            val updatedPerson = PersonDBCaretaker.updatePersonInformation(user, name, initials, surname, email, birthDate, birthPlace)
            return ResponseObject(SimplePerson.init(updatedPerson))
        } else {
            throw(MiniCouchException("Missing changes", ExceptionEnum.GENERIC))
        }
    }

    @RequestMapping(value = "/view_person", method = arrayOf(RequestMethod.GET))
    @Throws(MiniCouchException::class)
    fun view(@RequestParam(value = "token", required = true) t: String?
    ): ResponseObject<SimplePerson> {
        if (t != null) {
            val token = Token.decodeAndValidateToken(t)
            return ResponseObject(SimplePerson.init(PersonDBCaretaker.retrievePersonFromToken(token)))
        } else {
            throw(MiniCouchException("Missing changes", ExceptionEnum.GENERIC))
        }
    }


    @ExceptionHandler(MiniCouchException::class)
    fun handleAuthException(request: HttpServletRequest, ex: MiniCouchException): ExceptionResponse {
        return ExceptionResponse(request, ex)
    }

    @ExceptionHandler(Exception::class)
    fun handleAuthException(request: HttpServletRequest, ex: Exception): ExceptionResponse {
        return ExceptionResponse(request, ex)
    }
}