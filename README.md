# Mini Couch README #
Welcome to this initialisation document. 
Follow the steps below to get a working out of the box backend bank application. (Unless your school/company is behind a firewall, than it may take some more effort regarding downloading applications and cloning via Git)

## Requirements ##
* IntelliJ
* Git
* Java JDK 1.8 (JDK 1.9 is not yet supported!)
* Database (e.g. XAMPP)

## Acquire code ##
You can either use Git Bash or IntelliJ, recommended is IntelliJ but it really doesn�t matter much.

### Git Bash ###
git clone git@bitbucket.org:innovation_hub/backend.git

### IntelliJ ###
**Open** IntelliJ IDEA

![picture](readme/img/acquirecode_intellij_open.png)

**Select** Git at Check out from Version Control

![picture](readme/img/acquirecode_intellij_select.png)

|SET					| TO
|-----------------------|---------------------------------------------------|
|**Git repository URL** | git@bitbucket.org:innovation\_hub/backend.git		|
|**Parent Directory**	| *Use default or change to your preference.*		|
|**Directory Name** 	| *Use default or change to your preference.*		|


##Import##
If you have used Git Bash to clone, you must import it manually in IntelliJ.
**press** OK, after selecting the correct directory.

![picture](readme/img/import_select.png)

**Select Gradle** as import project from external model. 

![picture](readme/img/import_gradle.png)

Check or uncheck the boxes to your personal preference.

###Project JDK###

**Open** Project Structure (Ctrl+Alt+Shift+s)
**Set** Project JDK to 1.8, if needed create new.

![picture](readme/img/import_projectjdk.png)

##Build##

**Build**  by right clicking the build.gralde file and selecting "Run 'build'"
This will download all necessary libraries and plugins

![picture](readme/img/build_gradle_build.png)

##Gradle BootRun##
We are using BootRun to run our backend application locally.

This can be easily managed via IntelliJ's Edit Configurations.

![picture](readme/img/bootrun_edit_configurations.png)

**Create** a new Gradle Configuration

![picture](readme/img/bootrun_configuration_new.png)

| SET                          	| TO                                 	|
|------------------------------	|------------------------------------	|
| **Name**                     	| BootRun *(Or your preference)*     	|
| **Gradle Project**           	| *Select your project at the right* 	|
| **Tasks**                    	| bootRun                            	|
| **Environment Variables** \* 	| *server.port=8080*                 	|

*\*Default port = 8080, leave Environment Variables blank if that is Ok.*

![picture](readme/img/bootrun_configuration.png)

##BootRun Gradle##
**Run** the created Gradle.

**View** logging to be able and see if the program started correctly.
**Click** "Toggle task execution/text mode" icon at the top left of your console.
The last line should be something like: 
"2017-10-23 16:10:52.097  INFO 10964 --- [           main] com.abnamro.minicouch.ApplicationKt      : Started ApplicationKt in 3.639 seconds (JVM running for 4.02)"

All other logs will also be viewable here

![picture](readme/img/bootrun_logging.png)


**Install** Grep Console plugin optionally, if you prefer some colors in your logging.

Open settings with ctrl + alt + s
Select plugins and enter the information
Install plugin, restart Intellij after

![picture](readme/img/bootrun_logging_grep.png)


#Xamp#

##Download##

| **Download** 	|                                          	|
|--------------	|------------------------------------------	|
| **XAMPP**    	| https://www.apachefriends.org/index.html 	|

##Install##
**Check** required fields (MySQL & phpMyAdmin)

![picture](readme/img/xampp_install.png)

**Change** ports Apache (if necessary)

![picture](readme/img/xampp_install_config_change.png)

**Change** port to e.g. 8081 (if necessary)

![picture](readme/img/xampp_install_config_notepad.png)

###Database###
**Create** database in PhPMyAdmin (Xampp)

| SET               	| TO              	|
|-------------------	|-----------------	|
| **Database name** 	| mini\_couch      	|
| **Collation**     	| utf8_general_ci 	|

![picture](readme/img/database_create.png)

##Documentation##

###ERD###
ERD is placed within the git repository under (../GITROOT/erd/conceptual\_erd\_minicouch.architect)
And can be opened with SQL Power Architect

| **Download**                              |                                                          |
|-------------------------------------------|----------------------------------------------------------|
| **SQL Power architect Community Edition** | http://software.sqlpower.ca/page/architect\_download\_os | 

#Front End#
Test the code with our web or android application, or use a REST client like Postman.

##Web##

todo: Link to bitbucket

##Android##

todo: Link to bitbucket

##Postman##
todo: create project for postman as team or something
https://www.getpostman.com/collections/9ab0148c3371c6c1f35d **(Updated: 10-11-2017)**




